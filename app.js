var firebaseConfig = {
  apiKey: "AIzaSyCqhMwZKommg_Y5gilgvny9VKyJJEvBEr4",
  authDomain: "contactform-a2cf7.firebaseapp.com",
  databaseURL: "https://contactform-a2cf7.firebaseio.com",
  projectId: "contactform-a2cf7",
  storageBucket: "contactform-a2cf7.appspot.com",
  messagingSenderId: "387414977071",
  appId: "1:387414977071:web:c7e0cc80d16df7711034da",
  measurementId: "G-LPXQJ15GT3"
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();
var firestore = firebase.firestore();

const submitBtn = document.querySelector('#submit');

let userName = document.querySelector('#userFullName');
let userEmail = document.querySelector('#userEmail');
let userMessage = document.querySelector('#userMessage');

const db = firestore.collection('contactData');

submitBtn.addEventListener('click', function(){
  let userNameInput = userName.value;
  let userEmailInput = userEmail.value;
  let userMessageInput = userMessage.value;

  db.doc().set({
    name:userNameInput,
    email:userEmailInput,
    message:userMessageInput
  })
  .then(function(){
    alert("Data Saved");
  })
  .catch(function(error){
    console.log(error);
  });
});